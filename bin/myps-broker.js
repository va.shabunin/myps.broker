#!/usr/bin/env node

// commandline arguments
const argv = require("yargs")
  .option("sockfile", {
    alias: "s",
    describe: `path to socket file. Default: ${process.cwd()}/myipc.sock`
  })
  .option("debug", {
    alias: "d",
    describe: "debug output true/false"
  }).argv;

// using default params
let params = {
  debug: !!argv["debug"]
};

if (Object.prototype.hasOwnProperty.call(argv, "sockfile")) {
  params.socketFile = argv["sockfile"];
}

require("../lib/index.js")(params);
