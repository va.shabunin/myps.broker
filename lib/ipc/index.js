const net = require('net');
const fs = require('fs');
const EE = require('events').EventEmitter;

const FrameParser = require('./Parser');
const {composeFrame} = require('./compose');

// |B|D|S|M|<L>|<DATA>|<CR>
// <L> = <DATA>.length (~)
// <CR> = sum(<DATA>) % 256
let MyIPC = params => {
  let self = new EE();
  // default parameters
  let _params = {
    socketFile: `${process.env['XDG_RUNTIME_DIR']}/myipc.sock`,
    debug: true
  };
  Object.assign(_params, params);

  let _socketFile = _params.socketFile;
  let _debug = _params.debug;

  // ipc server
  let server;

  // array with clients
  let connections = [];

  self.debug = (...args) => {
    if (_debug) {
      console.log(`./lib/ipc/index:: ${args}`);
    }
  };

  // util func
  // prepare response before sending. useful when broadcasting
  const prepareResponse = objectToSend => {
    if (!Object.prototype.hasOwnProperty.call(objectToSend, 'method')) {
      throw new Error('Invalid object to send. It should has method field.');
    }
    if (!Object.prototype.hasOwnProperty.call(objectToSend, 'payload')) {
      throw new Error('Invalid object to send. It should has payload field.');
    }
    let data = {
      method: objectToSend.method,
      payload: objectToSend.payload
    };
    // now if data has response_id field
    if (Object.prototype.hasOwnProperty.call(objectToSend, 'response_id')) {
      data.response_id = objectToSend.response_id;
    }
    return JSON.stringify(data);
  };
  // now broadcasting
  self.broadcast = (data, ids) => {
    if (!(Array.isArray(ids) || typeof ids === 'number')) {
      throw new TypeError('Connection ids should be array or number');
    }
    // if we are dealing with one number then we wrap this number in array
    if (typeof ids === 'number') {
      return self.broadcast(data, [ids]);
    }
    let rawDataToSend;
    switch (typeof data) {
      case 'object':
        rawDataToSend = JSON.stringify(data);
        break;
      case 'string':
        rawDataToSend = data;
        break;
      default:
        rawDataToSend = data;
        break;
    }
    // helper func for Array.filter
    let filterByIds = t => {
      return ids.indexOf(t.id) > -1
    };
    let filtered = connections.filter(filterByIds);
    if (filtered.length > 0) {
      filtered.forEach(t => {
        t.write(rawDataToSend);
      })
    }
  };
  // now create server
  const createServer = socketFile => {
    return net
      .createServer(stream => {

        // write to socket functions
        //
        // send JSON object that should contains method and payload
        // may be used when we send single responses to requests
        // for broadcasting consider other funcs
        const writeObject = objectToSend => {
          stream.write(composeFrame(JSON.stringify(objectToSend)));
        };
        // if we are broadcasting, then we don't want to do a lot of checks and
        // JSON.stringify for every connection
        const writeString = stringToSend => {
          stream.write(composeFrame(stringToSend));
        };

        // overload for write func
        const write = data => {
          switch (typeof data) {
            case 'object':
              writeObject(data);
              break;
            case 'string':
              writeString(data);
              break;
            default:
              writeString(data);
              break;
          }
        };

        // generate unique connectionId
        let id;
        const findByConnId = t => {
          return t.id === id;
        };
        id = Math.round(Math.random() * Date.now());
        // if we can find connection with same id in array then generate new until id is unique
        while (connections.filter(findByConnId).length !== 0) {
          id = Math.round(Math.random() * Date.now());
        }

        let connection = {stream: stream, id: id, write: write};
        connections.push(connection);

        // now pipe incoming stream to bdsm parser
        const frameParser = new FrameParser();
        stream.pipe(frameParser);

        // error handling
        // emit event ipc
        self.emit('connected', connection);

        // DONE: (req, res), res as Proxy object
        frameParser.on('data', data => {
          try {
            // request is what we got from client
            let request = JSON.parse(data.toString());
            Object.assign(request, {connection_id: id});

            /** here I declare request and response proxy objects
             * to be transferred to 'request' event listeners
             *
             * (req, res) => {
             *   res.method = 'error';
             *   res.payload = 'runtime error';
             *   res.send();
             * }
             **/
            let requestProxy = new Proxy(request, {
              ownKeys: target => {
                return ['method', 'payload', 'connection_id'];
              },
              get: (obj, prop, receiver) => {
                if (prop === 'method' || prop === 'payload') {
                  return obj[prop];
                }
                if (prop === 'connection_id') {
                  return id;
                }

                // I restrict data fields to 'method' and 'payload' only fields
                // so, we don't want to return other props and methods yet
                return null;
              },
            });

            // now proceed to response
            let response = {};
            response['response_id'] = request['request_id'] || null;

            // exposed send method
            const send = _ => {
              let dataToSend = {};
              dataToSend['response_id'] = response['response_id'];
              dataToSend['method'] = response['method'];
              dataToSend['payload'] = response['payload'];
              connection.write(prepareResponse(dataToSend));
            };
            let responseProxy = new Proxy(response, {
              ownKeys: target => {
                return ['method', 'payload'];
              },
              get: (obj, prop, receiver) => {
                if (prop === 'send') {
                  return send;
                }
                if (prop === 'method' || prop === 'payload') {
                  return obj[prop];
                }

                // we don't want to return other props and methods yet
                return null;
              },
              set: (obj, prop, value, receiver) => {
                if (prop === 'method' || prop === 'payload') {
                  obj[prop] = value;
                }
              }
            });

            self.emit('request', requestProxy, responseProxy, id);
          } catch (e) {
            self.debug(e);
            self.emit('error', e);
            // TODO: send error response?
          }
        });

        // delete connection from connections array
        const forgetConnection = _ => {
          const findConnById = t => t.id === id;
          let connectionIndex = connections.findIndex(findConnById);

          if (connectionIndex >= 0) {
            self.emit('disconnect', id);
            connections.splice(connectionIndex, 1);
          }
        };
        stream.on('error', e => {
          self.debug(`error with socket ${id}: ${e.message}`);
          self.debug('disconnecting', id);
          forgetConnection();
        });
        stream.on('end', _ => {
          forgetConnection();
        });
      })
      .listen(socketFile);
  };

  self.debug('Checking for leftover socket.');
  fs.stat(_socketFile, function (err, stats) {
    if (err) {
      // start server
      self.debug('No leftover socket found.');
      self.debug(`Listening at  ${_socketFile}`);
      server = createServer(_socketFile);

      return;
    }
    // remove file then start server
    self.debug(`${_socketFile} already exist. Removing.`);
    fs.unlink(_socketFile, err => {
      if (err) {
        throw err;
      }
      self.debug(`Listening at ${_socketFile}`);
      server = createServer(_socketFile);
    });
  });

  // close connection
  self.close = _ => {
    server.close();
  };

  // now

  return self;
};

module.exports = MyIPC;
