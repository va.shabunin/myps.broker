const EE = require("events");
const MyIPC = require("./ipc/index.js");

// now create
const MyBroker = params => {
  let self = new EE();
  // default params.
  let _params = {
    socketFile: `${process.cwd()}/myipc.sock`,
    debug: true
  };

  Object.assign(_params, params);

  let _debug = _params.debug;
  self.debug = (...args) => {
    if (_debug) {
      console.log(`./lib/index:: ${args}`);
    }
  };

  // arrays with connection ids
  let connections = [];

  /** now array of topics
   * {
   *    name: 'hello',
   *    connections: [1, 2, 4, 8, 42]
   * }
   **/
  let topics = [];

  let _ipcParams = {
    socketFile: _params.socketFile,
    debug: _params.debug
  };
  let ipc = MyIPC(_ipcParams);

  ipc.on("connected", connection => {
    self.debug(`greeting ${connection.id}`);
    connection.write({ method: "greeting", payload: connection.id });
    if (connections.indexOf(connection) < 0) {
      connections.push(connection.id);
    }
  });

  ipc.on("disconnect", connection => {
    self.debug(`good bye ${connection}`);
    // unsubscribe from every topic
    let destroy = [];
    topics.forEach((t, i) => {
      let index = t.connections.indexOf(connection);
      if (index > -1) {
        t.connections.splice(index, 1);
      }
      // destroy topic if there is no more subscribers
      if (t.connections.length === 0) {
        self.debug(`No more subscribers in topic ${t.name}. Destroying.`);
        destroy.push(t.name);
      }
    });
    let withoutDestroyed = t => {
      return destroy.indexOf(t.name) === -1;
    };
    let newTopics = topics.filter(withoutDestroyed);

    topics = newTopics.slice();

    self.debug(`topics ${JSON.stringify(topics, null, 2)}`);
  });

  let subscribe = (topic, connection_id, cb) => {
    let findByTopic = t => t.name === topic;
    let topicsFound = topics.filter(findByTopic);
    if (topicsFound.length > 0) {
      // check if not subscribed yet
      topicsFound.forEach(t => {
        if (t.connections.indexOf(connection_id) < 0) {
          t.connections.push(connection_id);
          cb(null, "subscribe");
        } else {
          cb(new Error("Already subscribed"));
        }
        self.debug(`topics: ${JSON.stringify(topics, null, 2)}`);
      });
    } else {
      // there is no existing topic object, create new then
      let topicObject = {
        name: topic,
        connections: []
      };
      topicObject.connections.push(connection_id);
      topics.push(topicObject);
      cb(null, "subscribe");
      self.debug(`topics ${JSON.stringify(topics, null, 2)}`);
    }
  };
  let unsubscribe = (topic, connection_id, cb) => {
    let findByTopic = t => t.name === topic;
    let topicsFound = topics.filter(findByTopic);
    if (topicsFound.length > 0) {
      topicsFound.forEach(t => {
        let connectionIndex = t.connections.indexOf(connection_id);
        if (connectionIndex > -1) {
          t.connections.splice(connectionIndex, 1);

          // check if there is no more connections
          if (t.connections.length === 0) {
            // delete topic entirely
            let topicIndex = topics.findIndex(findByTopic);
            if (topicIndex > -1) {
              topics.splice(topicIndex, 1);
            }
          }
          cb(null, "unsubscribe");
        } else {
          cb(new Error("Not subscribed"));
        }
      });
    } else {
      cb(new Error("No topic found"));
    }
  };

  let publish = (topic, message, connection_id, cb) => {
    let findByTopic = t => t.name === topic;
    let topicsFound = topics.filter(findByTopic);
    if (topicsFound.length > 0) {
      topicsFound.forEach(t => {
        let ids = t.connections.slice();

        // remove connection_id from id array if subscribed
        // since I add connection_id field to payload it is not necessary
        // let connectionIdIndex = ids.indexOf(connection_id);
        // if (connectionIdIndex > -1) {
        //   ids.splice(connectionIdIndex, 1);
        // }

        // broadcast message
        let dataToSend = {};
        dataToSend.method = "message";
        dataToSend.payload = {};
        dataToSend.payload.topic = topic;
        dataToSend.payload.message = message;
        dataToSend.payload.connection_id = connection_id;
        ipc.broadcast(dataToSend, ids);
        cb(null, "publish");
      });
    } else {
      // no topic found but won't return error
      cb(null, "publish");
    }
  };

  // little echo test
  ipc.on("request", (req, res) => {
    self.debug(`req ${JSON.stringify(req, null, 2)}`);
    // let b = {};
    // b.method = req.method;
    // b.payload = req.payload;
    // ipc.broadcast(b, connections);
    let method = req.method;
    let payload = req.payload;
    let topic;
    let connection_id = req.connection_id;
    let findByTopic;
    switch (method) {
      case "subscribe":
        subscribe(payload, connection_id, (err, result) => {
          if (err) {
            res.method = "error";
            res.payload = err.message;
            self.debug(`res ${JSON.stringify(res, null, 2)}`);
            return res.send();
          }
          res.method = "success";
          res.payload = result;
          self.debug(`res ${JSON.stringify(res, null, 2)}`);
          res.send();
        });
        break;
      case "unsubscribe":
        unsubscribe(payload, connection_id, (err, result) => {
          if (err) {
            res.method = "error";
            res.payload = err.message;
            self.debug(`res ${JSON.stringify(res, null, 2)}`);
            return res.send();
          }
          res.method = "success";
          res.payload = result;
          self.debug(`res ${JSON.stringify(res, null, 2)}`);
          res.send();
        });
        break;
      case "publish":
        publish(payload.topic, payload.message, connection_id, (err, result) => {
          if (err) {
            res.method = "error";
            res.payload = err.message;
            self.debug(`res ${JSON.stringify(res, null, 2)}`);
            return res.send();
          }
          res.method = "success";
          res.payload = result;
          self.debug(`res ${JSON.stringify(res, null, 2)}`);
          res.send();
        });
        break;
      default:
        res.method = "error";
        res.payload = "Unknown method";
        self.debug(`res ${JSON.stringify(res, null, 2)}`);
        break;
    }
  });
};

module.exports = MyBroker;
